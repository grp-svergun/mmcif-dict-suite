/*
FILE:     DictParentChild.C
*/
/*
VERSION:  2.250
*/
/*
DATE:     10/21/2013
*/
/*
  Comments and Questions to: sw-help@rcsb.rutgers.edu
*/
/*
COPYRIGHT 1999-2013 Rutgers - The State University of New Jersey

This software is provided WITHOUT WARRANTY OF MERCHANTABILITY OR
FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER WARRANTY, EXPRESS OR
IMPLIED.  RUTGERS MAKE NO REPRESENTATION OR WARRANTY THAT THE
SOFTWARE WILL NOT INFRINGE ANY PATENT, COPYRIGHT OR OTHER
PROPRIETARY RIGHT.

The user of this software shall indemnify, hold harmless and defend
Rutgers, its governors, trustees, officers, employees, students,
agents and the authors against any and all claims, suits,
losses, liabilities, damages, costs, fees, and expenses including
reasonable attorneys' fees resulting from or arising out of the
use of this software.  This indemnification shall include, but is
not limited to, any and all claims alleging products liability.
*/
/*
               RCSB PDB SOFTWARE LICENSE AGREEMENT

BY CLICKING THE ACCEPTANCE BUTTON OR INSTALLING OR USING 
THIS "SOFTWARE, THE INDIVIDUAL OR ENTITY LICENSING THE  
SOFTWARE ("LICENSEE") IS CONSENTING TO BE BOUND BY AND IS 
BECOMING A PARTY TO THIS AGREEMENT.  IF LICENSEE DOES NOT 
AGREE TO ALL OF THE TERMS OF THIS AGREEMENT
THE LICENSEE MUST NOT INSTALL OR USE THE SOFTWARE.

1. LICENSE AGREEMENT

This is a license between you ("Licensee") and the Protein Data Bank (PDB) 
at Rutgers, The State University of New Jersey (hereafter referred to 
as "RUTGERS").   The software is owned by RUTGERS and protected by 
copyright laws, and some elements are protected by laws governing 
trademarks, trade dress and trade secrets, and may be protected by 
patent laws. 

2. LICENSE GRANT

RUTGERS grants you, and you hereby accept, non-exclusive, royalty-free 
perpetual license to install, use, modify, prepare derivative works, 
incorporate into other computer software, and distribute in binary 
and source code format, or any derivative work thereof, together with 
any associated media, printed materials, and on-line or electronic 
documentation (if any) provided by RUTGERS (collectively, the "SOFTWARE"), 
subject to the following terms and conditions: (i) any distribution 
of the SOFTWARE shall bind the receiver to the terms and conditions 
of this Agreement; (ii) any distribution of the SOFTWARE in modified 
form shall clearly state that the SOFTWARE has been modified from 
the version originally obtained from RUTGERS.  

2. COPYRIGHT; RETENTION OF RIGHTS.  

The above license grant is conditioned on the following: (i) you must 
reproduce all copyright notices and other proprietary notices on any 
copies of the SOFTWARE and you must not remove such notices; (ii) in 
the event you compile the SOFTWARE, you will include the copyright 
notice with the binary in such a manner as to allow it to be easily 
viewable; (iii) if you incorporate the SOFTWARE into other code, you 
must provide notice that the code contains the SOFTWARE and include 
a copy of the copyright notices and other proprietary notices.  All 
copies of the SOFTWARE shall be subject to the terms of this Agreement.  

3. NO MAINTENANCE OR SUPPORT; TREATMENT OF ENHANCEMENTS 

RUTGERS is under no obligation whatsoever to: (i) provide maintenance 
or support for the SOFTWARE; or (ii) to notify you of bug fixes, patches, 
or upgrades to the features, functionality or performance of the 
SOFTWARE ("Enhancements") (if any), whether developed by RUTGERS 
or third parties.  If, in its sole discretion, RUTGERS makes an 
Enhancement available to you and RUTGERS does not separately enter 
into a written license agreement with you relating to such bug fix, 
patch or upgrade, then it shall be deemed incorporated into the SOFTWARE 
and subject to this Agreement. You are under no obligation whatsoever 
to provide any Enhancements to RUTGERS or the public that you may 
develop over time; however, if you choose to provide your Enhancements 
to RUTGERS, or if you choose to otherwise publish or distribute your 
Enhancements, in source code form without contemporaneously requiring 
end users or RUTGERS to enter into a separate written license agreement 
for such Enhancements, then you hereby grant RUTGERS a non-exclusive,
royalty-free perpetual license to install, use, modify, prepare
derivative works, incorporate into the SOFTWARE or other computer
software, distribute, and sublicense your Enhancements or derivative
works thereof, in binary and source code form.

4. FEES.  There is no license fee for the SOFTWARE.  If Licensee
wishes to receive the SOFTWARE on media, there may be a small charge
for the media and for shipping and handling.  Licensee is
responsible for any and all taxes.

5. TERMINATION.  Without prejudice to any other rights, Licensor
may terminate this Agreement if Licensee breaches any of its terms
and conditions.  Upon termination, Licensee shall destroy all
copies of the SOFTWARE.

6. PROPRIETARY RIGHTS.  Title, ownership rights, and intellectual
property rights in the Product shall remain with RUTGERS.  Licensee 
acknowledges such ownership and intellectual property rights and will 
not take any action to jeopardize, limit or interfere in any manner 
with RUTGERS' ownership of or rights with respect to the SOFTWARE.  
The SOFTWARE is protected by copyright and other intellectual 
property laws and by international treaties.  Title and related 
rights in the content accessed through the SOFTWARE is the property 
of the applicable content owner and is protected by applicable law.  
The license granted under this Agreement gives Licensee no rights to such
content.

7. DISCLAIMER OF WARRANTY.  THE SOFTWARE IS PROVIDED FREE OF 
CHARGE, AND, THEREFORE, ON AN "AS IS" BASIS, WITHOUT WARRANTY OF 
ANY KIND, INCLUDING WITHOUT LIMITATION THE WARRANTIES THAT IT 
IS FREE OF DEFECTS, MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE 
OR NON-INFRINGING.  THE ENTIRE RISK AS TO THE QUALITY AND 
PERFORMANCE OF THE SOFTWARE IS BORNE BY LICENSEE.  SHOULD THE 
SOFTWARE PROVE DEFECTIVE IN ANY RESPECT, THE LICENSEE AND NOT 
LICENSOR ASSUMES THE ENTIRE COST OF ANY SERVICE AND REPAIR.  
THIS DISCLAIMER OF WARRANTY CONSTITUTES AN ESSENTIAL PART OF 
THIS AGREEMENT.  NO USE OF THE PRODUCT IS AUTHORIZED HEREUNDER 
EXCEPT UNDER THIS DISCLAIMER.

8. LIMITATION OF LIABILITY.  TO THE MAXIMUM EXTENT PERMITTED BY
APPLICABLE LAW,  IN NO EVENT WILL LICENSOR BE LIABLE FOR ANY 
INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING 
OUT OF THE USE OF OR INABILITY TO USE THE SOFTWARE, INCLUDING, 
WITHOUT LIMITATION, DAMAGES FOR LOSS OF GOODWILL, WORK 
STOPPAGE, COMPUTER FAILURE OR MALFUNCTION, OR ANY AND ALL 
OTHER COMMERCIAL DAMAGES OR LOSSES, EVEN IF ADVISED OF THE
POSSIBILITY THEREOF. 
*/


/*!
** \file DictParentChild.C
**
** \brief Implementation file for DictParentChild class.
*/


#include <string>
#include <vector>
#include <algorithm>

#include "GenString.h"
#include "GenCont.h"
#include "CifString.h"
#include "ISTable.h"
#include "TableFile.h"
#include "DictParentChild.h"


using std::string;
using std::vector;
using std::cout;
using std::endl;
using std::sort;


DictParentChild::DictParentChild(const DictObjCont& dictObjCont,
  DictDataInfo& dictDataInfo) : ParentChild(), _dictObjCont(dictObjCont),
  _dictDataInfo(dictDataInfo)
{
    FillGroupTable(*_groupTableP);
    FillGroupListTable(*_groupListTableP, *_groupTableP);

    // Add one more column to the group table, that will tell the parent
    // category of the group.
    AddParentCategoryToItemLinkedGroup(*_groupTableP, *_groupListTableP);

    // Now create relations
    CreateAllRelations(*_groupTableP, *_groupListTableP);
}


DictParentChild::~DictParentChild()
{

}


const DictObjCont& DictParentChild::GetDictObjCont()
{
    return (_dictObjCont);
}


void DictParentChild::GetParentCifItems(vector<string>& parCifItems,
  const string& cifItemName)
{
    _dictDataInfo.GetParentCifItems(parCifItems, cifItemName);
}


void DictParentChild::FillGroupTable(ISTable& groupTable)
{
    vector<string> cats = _dictDataInfo.GetCatNames();

    sort(cats.begin(), cats.end());

    for (unsigned int catI = 0; catI < cats.size(); ++catI)
    {
        const vector<string>& linkGroupId = _dictDataInfo.GetCatAttribute(
          cats[catI], CifString::CIF_DDL_CATEGORY_PDBX_ITEM_LINKED_GROUP,
          CifString::CIF_DDL_ITEM_LINK_GROUP_ID);

        const vector<string>& label = _dictDataInfo.GetCatAttribute(
          cats[catI], CifString::CIF_DDL_CATEGORY_PDBX_ITEM_LINKED_GROUP,
          CifString::CIF_DDL_ITEM_LABEL);

        const vector<string>& context = _dictDataInfo.GetCatAttribute(
          cats[catI], CifString::CIF_DDL_CATEGORY_PDBX_ITEM_LINKED_GROUP,
          CifString::CIF_DDL_ITEM_CONTEXT);

        const vector<string>& conditionId = _dictDataInfo.GetCatAttribute(
          cats[catI], CifString::CIF_DDL_CATEGORY_PDBX_ITEM_LINKED_GROUP,
          CifString::CIF_DDL_ITEM_CONDITION_ID);

        for (unsigned int numI = 0; numI < linkGroupId.size(); ++numI)
        {
            vector<string> newGroupRow;
            newGroupRow.push_back(cats[catI]);
            newGroupRow.push_back(linkGroupId[numI]);
            newGroupRow.push_back(label[numI]);
            newGroupRow.push_back(context[numI]);
            newGroupRow.push_back(conditionId[numI]);
            groupTable.AddRow(newGroupRow);
        } // for (every group in category)
    } // for (every category acting as a child)
}


void DictParentChild::FillGroupListTable(ISTable& groupListTable,
  ISTable& groupTable)
{
    vector<string> cats = _dictDataInfo.GetCatNames();

    sort(cats.begin(), cats.end());

    for (unsigned int catI = 0; catI < cats.size(); ++catI)
    {
        const vector<string>& linkGroupId = _dictDataInfo.GetCatAttribute(
          cats[catI], CifString::CIF_DDL_CATEGORY_PDBX_ITEM_LINKED_GROUP,
          CifString::CIF_DDL_ITEM_LINK_GROUP_ID);

        unsigned int nextGroupInt = linkGroupId.size() + 1;

        vector<string> cifItemNames;
        _dictDataInfo.GetCatItemsNames(cifItemNames, cats[catI]);

        for (unsigned int childI = 0; childI < cifItemNames.size(); ++childI)
        {
            const vector<string>& parItemNames =
              _dictDataInfo.GetItemAttribute(cifItemNames[childI],
              CifString::CIF_DDL_CATEGORY_PDBX_ITEM_LINKED_GROUP_LIST,
              CifString::CIF_DDL_ITEM_PARENT_NAME);

            const vector<string>& parCatNames =
              _dictDataInfo.GetItemAttribute(cifItemNames[childI],
              CifString::CIF_DDL_CATEGORY_PDBX_ITEM_LINKED_GROUP_LIST,
              CifString::CIF_DDL_ITEM_PARENT_CATEGORY_ID);

            const vector<string>& linkGroupId =
              _dictDataInfo.GetItemAttribute(cifItemNames[childI],
              CifString::CIF_DDL_CATEGORY_PDBX_ITEM_LINKED_GROUP_LIST,
              CifString::CIF_DDL_ITEM_LINK_GROUP_ID);

            for (unsigned int parI = 0; parI < parItemNames.size(); ++parI)
            {
                if (!_dictDataInfo.IsItemDefined(parItemNames[parI]))
                {
                    cout << "Warning: Child category \"" << cats[catI] <<
                      "\", child item \"" << cifItemNames[childI] <<
                      "\", link group \"" << linkGroupId[parI] <<
                      "\" is associated with non-defined parent item \"" <<
                      parItemNames[parI] << "\" and this entry will be "\
                      "ignored and not stored in \"" <<
                      groupListTable.GetName() << "\" table." << endl;
                    continue;
                }

                vector<string> row;

                row.push_back(cats[catI]);

                const string& linkGroup = linkGroupId[parI];
                row.push_back(linkGroup);

                row.push_back(cifItemNames[childI]);

                row.push_back(parItemNames[parI]);
      
                row.push_back(parCatNames[parI]);

                groupListTable.AddRow(row);
            } // for (every row where child CIF item is present)
        } // for (all child items)

        vector<string> childSearchCol;
        childSearchCol.push_back("child_name");

        for (unsigned int childI = 0; childI < cifItemNames.size(); ++childI)
        {
            // Check if item has already been processed
            vector<string> childTarget;
            childTarget.push_back(cifItemNames[childI]);

            unsigned int childRow = groupListTable.FindFirst(childTarget,
              childSearchCol);

            if (childRow != groupListTable.GetNumRows())
            {
                continue;
            }

            const vector<string>& parCifItems =
              _dictDataInfo.GetItemAttribute(cifItemNames[childI],
              CifString::CIF_DDL_CATEGORY_ITEM_LINKED,
              CifString::CIF_DDL_ITEM_PARENT_NAME);

            for (unsigned int parI = 0; parI < parCifItems.size(); ++parI,
              ++nextGroupInt)
            {
                const string& parentItem = parCifItems[parI];

                string parCatName;
                CifString::GetCategoryFromCifItem(parCatName, parentItem);

                if (!_dictDataInfo.IsCatDefined(parCatName))
                {
                    cout << "Warning: Child item \"" << cifItemNames[childI] <<
                      "\" is associated, via item_linked, with non-defined "\
                      "parent item \"" << parentItem <<
                      "\" and this entry will be ignored." << endl;
                    continue;
                }

                cout << "Info: Creating a new group \"" <<
                  String::IntToString(nextGroupInt) << "\" for child "\
                  "category \"" << cats[catI] << "\" for child item \"" <<
                  cifItemNames[childI] << "\" and parent item \"" <<
                  parentItem << "\", from \"item_linked\" table, as these "\
                  "are not defined in group tables." << endl;

                vector<string> row;

                row.push_back(cats[catI]);

                row.push_back(String::IntToString(nextGroupInt));

                row.push_back(cifItemNames[childI]);

                row.push_back(parentItem);
  
                row.push_back(parCatName);

                groupListTable.AddRow(row);

                vector<string> newGroupRow;
                newGroupRow.push_back(cats[catI]);
                newGroupRow.push_back(String::IntToString(nextGroupInt));
                newGroupRow.push_back(cats[catI] + ":" + parCatName + ":" +
                  String::IntToString(nextGroupInt));
                newGroupRow.push_back(CifString::InapplicableValue);
                newGroupRow.push_back(CifString::InapplicableValue);
                groupTable.AddRow(newGroupRow);
            }
        } // for (all child items)
    }
}


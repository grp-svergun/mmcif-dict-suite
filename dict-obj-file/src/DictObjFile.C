/*
FILE:     DictObjFile.C
*/
/*
VERSION:  2.250
*/
/*
DATE:     10/21/2013
*/
/*
  Comments and Questions to: sw-help@rcsb.rutgers.edu
*/
/*
COPYRIGHT 1999-2013 Rutgers - The State University of New Jersey

This software is provided WITHOUT WARRANTY OF MERCHANTABILITY OR
FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER WARRANTY, EXPRESS OR
IMPLIED.  RUTGERS MAKE NO REPRESENTATION OR WARRANTY THAT THE
SOFTWARE WILL NOT INFRINGE ANY PATENT, COPYRIGHT OR OTHER
PROPRIETARY RIGHT.

The user of this software shall indemnify, hold harmless and defend
Rutgers, its governors, trustees, officers, employees, students,
agents and the authors against any and all claims, suits,
losses, liabilities, damages, costs, fees, and expenses including
reasonable attorneys' fees resulting from or arising out of the
use of this software.  This indemnification shall include, but is
not limited to, any and all claims alleging products liability.
*/
/*
               RCSB PDB SOFTWARE LICENSE AGREEMENT

BY CLICKING THE ACCEPTANCE BUTTON OR INSTALLING OR USING 
THIS "SOFTWARE, THE INDIVIDUAL OR ENTITY LICENSING THE  
SOFTWARE ("LICENSEE") IS CONSENTING TO BE BOUND BY AND IS 
BECOMING A PARTY TO THIS AGREEMENT.  IF LICENSEE DOES NOT 
AGREE TO ALL OF THE TERMS OF THIS AGREEMENT
THE LICENSEE MUST NOT INSTALL OR USE THE SOFTWARE.

1. LICENSE AGREEMENT

This is a license between you ("Licensee") and the Protein Data Bank (PDB) 
at Rutgers, The State University of New Jersey (hereafter referred to 
as "RUTGERS").   The software is owned by RUTGERS and protected by 
copyright laws, and some elements are protected by laws governing 
trademarks, trade dress and trade secrets, and may be protected by 
patent laws. 

2. LICENSE GRANT

RUTGERS grants you, and you hereby accept, non-exclusive, royalty-free 
perpetual license to install, use, modify, prepare derivative works, 
incorporate into other computer software, and distribute in binary 
and source code format, or any derivative work thereof, together with 
any associated media, printed materials, and on-line or electronic 
documentation (if any) provided by RUTGERS (collectively, the "SOFTWARE"), 
subject to the following terms and conditions: (i) any distribution 
of the SOFTWARE shall bind the receiver to the terms and conditions 
of this Agreement; (ii) any distribution of the SOFTWARE in modified 
form shall clearly state that the SOFTWARE has been modified from 
the version originally obtained from RUTGERS.  

2. COPYRIGHT; RETENTION OF RIGHTS.  

The above license grant is conditioned on the following: (i) you must 
reproduce all copyright notices and other proprietary notices on any 
copies of the SOFTWARE and you must not remove such notices; (ii) in 
the event you compile the SOFTWARE, you will include the copyright 
notice with the binary in such a manner as to allow it to be easily 
viewable; (iii) if you incorporate the SOFTWARE into other code, you 
must provide notice that the code contains the SOFTWARE and include 
a copy of the copyright notices and other proprietary notices.  All 
copies of the SOFTWARE shall be subject to the terms of this Agreement.  

3. NO MAINTENANCE OR SUPPORT; TREATMENT OF ENHANCEMENTS 

RUTGERS is under no obligation whatsoever to: (i) provide maintenance 
or support for the SOFTWARE; or (ii) to notify you of bug fixes, patches, 
or upgrades to the features, functionality or performance of the 
SOFTWARE ("Enhancements") (if any), whether developed by RUTGERS 
or third parties.  If, in its sole discretion, RUTGERS makes an 
Enhancement available to you and RUTGERS does not separately enter 
into a written license agreement with you relating to such bug fix, 
patch or upgrade, then it shall be deemed incorporated into the SOFTWARE 
and subject to this Agreement. You are under no obligation whatsoever 
to provide any Enhancements to RUTGERS or the public that you may 
develop over time; however, if you choose to provide your Enhancements 
to RUTGERS, or if you choose to otherwise publish or distribute your 
Enhancements, in source code form without contemporaneously requiring 
end users or RUTGERS to enter into a separate written license agreement 
for such Enhancements, then you hereby grant RUTGERS a non-exclusive,
royalty-free perpetual license to install, use, modify, prepare
derivative works, incorporate into the SOFTWARE or other computer
software, distribute, and sublicense your Enhancements or derivative
works thereof, in binary and source code form.

4. FEES.  There is no license fee for the SOFTWARE.  If Licensee
wishes to receive the SOFTWARE on media, there may be a small charge
for the media and for shipping and handling.  Licensee is
responsible for any and all taxes.

5. TERMINATION.  Without prejudice to any other rights, Licensor
may terminate this Agreement if Licensee breaches any of its terms
and conditions.  Upon termination, Licensee shall destroy all
copies of the SOFTWARE.

6. PROPRIETARY RIGHTS.  Title, ownership rights, and intellectual
property rights in the Product shall remain with RUTGERS.  Licensee 
acknowledges such ownership and intellectual property rights and will 
not take any action to jeopardize, limit or interfere in any manner 
with RUTGERS' ownership of or rights with respect to the SOFTWARE.  
The SOFTWARE is protected by copyright and other intellectual 
property laws and by international treaties.  Title and related 
rights in the content accessed through the SOFTWARE is the property 
of the applicable content owner and is protected by applicable law.  
The license granted under this Agreement gives Licensee no rights to such
content.

7. DISCLAIMER OF WARRANTY.  THE SOFTWARE IS PROVIDED FREE OF 
CHARGE, AND, THEREFORE, ON AN "AS IS" BASIS, WITHOUT WARRANTY OF 
ANY KIND, INCLUDING WITHOUT LIMITATION THE WARRANTIES THAT IT 
IS FREE OF DEFECTS, MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE 
OR NON-INFRINGING.  THE ENTIRE RISK AS TO THE QUALITY AND 
PERFORMANCE OF THE SOFTWARE IS BORNE BY LICENSEE.  SHOULD THE 
SOFTWARE PROVE DEFECTIVE IN ANY RESPECT, THE LICENSEE AND NOT 
LICENSOR ASSUMES THE ENTIRE COST OF ANY SERVICE AND REPAIR.  
THIS DISCLAIMER OF WARRANTY CONSTITUTES AN ESSENTIAL PART OF 
THIS AGREEMENT.  NO USE OF THE PRODUCT IS AUTHORIZED HEREUNDER 
EXCEPT UNDER THIS DISCLAIMER.

8. LIMITATION OF LIABILITY.  TO THE MAXIMUM EXTENT PERMITTED BY
APPLICABLE LAW,  IN NO EVENT WILL LICENSOR BE LIABLE FOR ANY 
INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING 
OUT OF THE USE OF OR INABILITY TO USE THE SOFTWARE, INCLUDING, 
WITHOUT LIMITATION, DAMAGES FOR LOSS OF GOODWILL, WORK 
STOPPAGE, COMPUTER FAILURE OR MALFUNCTION, OR ANY AND ALL 
OTHER COMMERCIAL DAMAGES OR LOSSES, EVEN IF ADVISED OF THE
POSSIBILITY THEREOF. 
*/


/*!
** \file DictObjFile.C
**
** \brief Implementation file for DictObjFile class.
*/


#include <string>
#include <algorithm>

#include "RcsbFile.h"
#include "ISTable.h"
#include "CifFileUtil.h"
#include "DictObjFile.h"


using std::make_pair;
using std::cout;
using std::endl;
using std::sort;


string OdbFileVersion("V1");


DictObjFile::DictObjFile(const string& persStorFileName,
  const eFileMode fileMode, const bool verbose, const string& dictSdbFileName)
  : _verbose(verbose), _dicFileP(NULL),
  _ser(*(new Serializer(persStorFileName, fileMode, verbose))),
  _currDictObjContP(NULL)
{
    if ((fileMode != READ_MODE) && (fileMode != CREATE_MODE))
    {
        throw FileModeException("Dictionary object file not in read or "\
          "create mode", "DictObjFile::DictObjFile()");
    }

    if (fileMode == READ_MODE)
    {
        if (!dictSdbFileName.empty())
        {
            throw InvalidStateException("Non-empty dictionary file name "\
              "in read mode", "DictObjFile::DictObjFile()");
        }
    }

    if (fileMode == CREATE_MODE)
    {
        if (dictSdbFileName.empty())
        {
            throw EmptyValueException("Empty dictionary file name in "\
              "create mode", "DictObjFile::DictObjFile()");
        }
    }

    _fileMode = fileMode;
    _dictSdbFileName = dictSdbFileName;
}


DictObjFile::~DictObjFile()
{
    _dictSdbFileName.clear();

    delete (_dicFileP);

    delete &(_ser);

    if (!_dictionaries.empty())
    {
      for (unsigned int j = 0; j < _dictionaries.size(); ++j)
        delete(&(_dictionaries[j]));
      _dictionaries.clear();
    }

}


void DictObjFile::Build()
{
    if (_fileMode != CREATE_MODE)
    {
        throw FileModeException("Dictionary object file not in create mode",
          "DictObjFile::Build()");
    }

    _dicFileP = GetDictFile(NULL, string(), _dictSdbFileName);

    vector<string> dictNames;
    _dicFileP->GetBlockNames(dictNames);

    _dictionaries.push_back(dictNames);
}


void DictObjFile::Write()
{

    if (_fileMode != CREATE_MODE)
    {
        throw FileModeException("Dictionary object file not in create mode",
          "DictObjFile::Write()");
    }

    vector<string> blockNames;
    GetDictionaryNames(blockNames);

    cout << "Number of dictionary datablocks: " << blockNames.size() << endl;

    // Write ODB file version
    // HARDCODED - VLAD - FORMAT DEPENDENT
    _ser.WriteString(OdbFileVersion);
    _ser.WriteUInt32(23);

    sort(blockNames.begin(), blockNames.end());

    vector<UInt32> list;
    for (unsigned int i = 0; i < blockNames.size(); i++)
    {
        DictObjCont* dictObjContP = new DictObjCont(_ser, *_dicFileP,
          blockNames[i]);

        dictObjContP->SetVerbose(_verbose);

        cout << "Building dictionary object: " << blockNames[i] << endl;
        dictObjContP->Build();

        if (_verbose)
        {
            cout << "Printing dictionary object: " << blockNames[i] << endl;
            dictObjContP->Print();
        }

        cout << "Writing dictionary object: " << blockNames[i] << endl;
        UInt32 ret = dictObjContP->Write();

        if (ret > 0)
        {
            list.push_back(ret);
        }
        else
        {
            cout << "Write dictionary object error code  = " << ret << endl;
            list.push_back(0);
        }
        delete dictObjContP;
    }

    UInt32 ret = _ser.WriteUInt32s(list);

    _ser.UpdateUInt32(ret, 1);

    cout << "Last index = " << ret << endl;

    _ser.WriteStrings(blockNames);

    cout << "Return code = " << 0 << endl;

}


void DictObjFile::Read()
{

    if (_fileMode != READ_MODE)
    {
        throw FileModeException("Dictionary object file not in read mode",
          "DictObjFile::Read()");
    }

    string firstString;
    // HARDCODED - VLAD - FORMAT DEPENDENT
    _ser.ReadString(firstString, 0);
    if (firstString != OdbFileVersion)
    {
        throw VersionMismatchException("Cannot read old versions of "\
          "dictionary object file!", "DictObjFile::Read");
    }

    // HARDCODED - VLAD - FORMAT DEPENDENT
    // Location of the dictionary indices in the file.
    UInt32 location = _ser.ReadUInt32(1);

    if (location == 0)
        // VLAD - ERROR HANDLING return BUILD_LIST_ERROR;
        return;

    vector<UInt32> dictIndices;
    _ser.ReadUInt32s(dictIndices, location);

    vector<string> dictNames;
    // HARDCODED - VLAD - FORMAT DEPENDENT
    _ser.ReadStrings(dictNames, location + 1);

    if (dictIndices.size() != dictNames.size())
        return;
        // return BUILD_LIST_ERROR;

    _dictionaries.push_back(dictNames, dictIndices);

}


unsigned int DictObjFile::GetNumDictionaries()
{
    return(_dictionaries.size());
}


void DictObjFile::GetDictionaryNames(vector<string>& dictNames)
{
    dictNames.clear();

    if (!_dictSdbFileName.empty())
    {
        _dicFileP->GetBlockNames(dictNames);
    }
    else
    {
        for (unsigned int index = 0; index < _dictionaries.size(); ++index)
        {
            dictNames.push_back(_dictionaries[index].GetName());
        }
    }
}


DictObjCont& DictObjFile::GetDictObjCont(const string& dictName)
{

    if (_currDictObjContP != NULL)
    {
        if (_currDictObjContP->GetName() == dictName)
        {
            return(*_currDictObjContP);
        }
    }

    unsigned int l = _dictionaries.find(dictName);
    if (l == _dictionaries.size())
    {
        throw NotFoundException("Dictionary \"" + dictName + "\" not found.",
          "DictObjFile::GetDictObjCont");
    }

    if (!_dictionaries.is_read(dictName))
    {
        _dictionaries.set(new DictObjCont(_ser, *_dicFileP, dictName));
        _dictionaries.read(dictName);
    }

    _currDictObjContP = &(_dictionaries[l]);

    return(*_currDictObjContP);

}


void DictObjFile::Print()
{
    for (unsigned int dictI = 0; dictI < _dictionaries.size(); ++dictI)
    {
        DictObjCont& dictObjCont = GetDictObjCont(_dictionaries[dictI].\
          GetName());

        dictObjCont.Print();    
    }
}


/*
FILE:     SchemaMap.h
*/
/*
VERSION:  2.250
*/
/*
DATE:     10/21/2013
*/
/*
  Comments and Questions to: sw-help@rcsb.rutgers.edu
*/
/*
COPYRIGHT 1999-2013 Rutgers - The State University of New Jersey

This software is provided WITHOUT WARRANTY OF MERCHANTABILITY OR
FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER WARRANTY, EXPRESS OR
IMPLIED.  RUTGERS MAKE NO REPRESENTATION OR WARRANTY THAT THE
SOFTWARE WILL NOT INFRINGE ANY PATENT, COPYRIGHT OR OTHER
PROPRIETARY RIGHT.

The user of this software shall indemnify, hold harmless and defend
Rutgers, its governors, trustees, officers, employees, students,
agents and the authors against any and all claims, suits,
losses, liabilities, damages, costs, fees, and expenses including
reasonable attorneys' fees resulting from or arising out of the
use of this software.  This indemnification shall include, but is
not limited to, any and all claims alleging products liability.
*/
/*
               RCSB PDB SOFTWARE LICENSE AGREEMENT

BY CLICKING THE ACCEPTANCE BUTTON OR INSTALLING OR USING 
THIS "SOFTWARE, THE INDIVIDUAL OR ENTITY LICENSING THE  
SOFTWARE ("LICENSEE") IS CONSENTING TO BE BOUND BY AND IS 
BECOMING A PARTY TO THIS AGREEMENT.  IF LICENSEE DOES NOT 
AGREE TO ALL OF THE TERMS OF THIS AGREEMENT
THE LICENSEE MUST NOT INSTALL OR USE THE SOFTWARE.

1. LICENSE AGREEMENT

This is a license between you ("Licensee") and the Protein Data Bank (PDB) 
at Rutgers, The State University of New Jersey (hereafter referred to 
as "RUTGERS").   The software is owned by RUTGERS and protected by 
copyright laws, and some elements are protected by laws governing 
trademarks, trade dress and trade secrets, and may be protected by 
patent laws. 

2. LICENSE GRANT

RUTGERS grants you, and you hereby accept, non-exclusive, royalty-free 
perpetual license to install, use, modify, prepare derivative works, 
incorporate into other computer software, and distribute in binary 
and source code format, or any derivative work thereof, together with 
any associated media, printed materials, and on-line or electronic 
documentation (if any) provided by RUTGERS (collectively, the "SOFTWARE"), 
subject to the following terms and conditions: (i) any distribution 
of the SOFTWARE shall bind the receiver to the terms and conditions 
of this Agreement; (ii) any distribution of the SOFTWARE in modified 
form shall clearly state that the SOFTWARE has been modified from 
the version originally obtained from RUTGERS.  

2. COPYRIGHT; RETENTION OF RIGHTS.  

The above license grant is conditioned on the following: (i) you must 
reproduce all copyright notices and other proprietary notices on any 
copies of the SOFTWARE and you must not remove such notices; (ii) in 
the event you compile the SOFTWARE, you will include the copyright 
notice with the binary in such a manner as to allow it to be easily 
viewable; (iii) if you incorporate the SOFTWARE into other code, you 
must provide notice that the code contains the SOFTWARE and include 
a copy of the copyright notices and other proprietary notices.  All 
copies of the SOFTWARE shall be subject to the terms of this Agreement.  

3. NO MAINTENANCE OR SUPPORT; TREATMENT OF ENHANCEMENTS 

RUTGERS is under no obligation whatsoever to: (i) provide maintenance 
or support for the SOFTWARE; or (ii) to notify you of bug fixes, patches, 
or upgrades to the features, functionality or performance of the 
SOFTWARE ("Enhancements") (if any), whether developed by RUTGERS 
or third parties.  If, in its sole discretion, RUTGERS makes an 
Enhancement available to you and RUTGERS does not separately enter 
into a written license agreement with you relating to such bug fix, 
patch or upgrade, then it shall be deemed incorporated into the SOFTWARE 
and subject to this Agreement. You are under no obligation whatsoever 
to provide any Enhancements to RUTGERS or the public that you may 
develop over time; however, if you choose to provide your Enhancements 
to RUTGERS, or if you choose to otherwise publish or distribute your 
Enhancements, in source code form without contemporaneously requiring 
end users or RUTGERS to enter into a separate written license agreement 
for such Enhancements, then you hereby grant RUTGERS a non-exclusive,
royalty-free perpetual license to install, use, modify, prepare
derivative works, incorporate into the SOFTWARE or other computer
software, distribute, and sublicense your Enhancements or derivative
works thereof, in binary and source code form.

4. FEES.  There is no license fee for the SOFTWARE.  If Licensee
wishes to receive the SOFTWARE on media, there may be a small charge
for the media and for shipping and handling.  Licensee is
responsible for any and all taxes.

5. TERMINATION.  Without prejudice to any other rights, Licensor
may terminate this Agreement if Licensee breaches any of its terms
and conditions.  Upon termination, Licensee shall destroy all
copies of the SOFTWARE.

6. PROPRIETARY RIGHTS.  Title, ownership rights, and intellectual
property rights in the Product shall remain with RUTGERS.  Licensee 
acknowledges such ownership and intellectual property rights and will 
not take any action to jeopardize, limit or interfere in any manner 
with RUTGERS' ownership of or rights with respect to the SOFTWARE.  
The SOFTWARE is protected by copyright and other intellectual 
property laws and by international treaties.  Title and related 
rights in the content accessed through the SOFTWARE is the property 
of the applicable content owner and is protected by applicable law.  
The license granted under this Agreement gives Licensee no rights to such
content.

7. DISCLAIMER OF WARRANTY.  THE SOFTWARE IS PROVIDED FREE OF 
CHARGE, AND, THEREFORE, ON AN "AS IS" BASIS, WITHOUT WARRANTY OF 
ANY KIND, INCLUDING WITHOUT LIMITATION THE WARRANTIES THAT IT 
IS FREE OF DEFECTS, MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE 
OR NON-INFRINGING.  THE ENTIRE RISK AS TO THE QUALITY AND 
PERFORMANCE OF THE SOFTWARE IS BORNE BY LICENSEE.  SHOULD THE 
SOFTWARE PROVE DEFECTIVE IN ANY RESPECT, THE LICENSEE AND NOT 
LICENSOR ASSUMES THE ENTIRE COST OF ANY SERVICE AND REPAIR.  
THIS DISCLAIMER OF WARRANTY CONSTITUTES AN ESSENTIAL PART OF 
THIS AGREEMENT.  NO USE OF THE PRODUCT IS AUTHORIZED HEREUNDER 
EXCEPT UNDER THIS DISCLAIMER.

8. LIMITATION OF LIABILITY.  TO THE MAXIMUM EXTENT PERMITTED BY
APPLICABLE LAW,  IN NO EVENT WILL LICENSOR BE LIABLE FOR ANY 
INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING 
OUT OF THE USE OF OR INABILITY TO USE THE SOFTWARE, INCLUDING, 
WITHOUT LIMITATION, DAMAGES FOR LOSS OF GOODWILL, WORK 
STOPPAGE, COMPUTER FAILURE OR MALFUNCTION, OR ANY AND ALL 
OTHER COMMERCIAL DAMAGES OR LOSSES, EVEN IF ADVISED OF THE
POSSIBILITY THEREOF. 
*/


/*!
** \file SchemaMap.h
**
** \brief Header file for SchemaMap related classes.
*/


#ifndef SCHEMAMAP_H
#define SCHEMAMAP_H


#include <string>

#include "GenString.h"
#include "CifFile.h"
#include "DicFile.h"

typedef struct
{
    string attribName;
    string dataType;
    eTypeCode iTypeCode; // from dataType
    bool iIndex;
    int iNull;
    int iWidth;
    int iPrecision;
    string populated;   // Not taken from the file, but constructed afterwards
} AttrInfo;


/**
**  \class SchemaMap
**
**  \brief Public class that respresents schema mapping configuration.
**
**  This class encapsulates schema mapping functionality. Schema map
**  defines how mmCIF data gets converted into the database data. Schema
**  map information is stored in a number of configuration tables
**  that are located in a configuration CIF file. This class provides methods
**  for reading the schema mapping information from the ASCII CIF file or
**  serialized (binary) configuration CIF file, for revising the schema
**  mapping information (based on the data), updating the schema mapping
**  information, accessing the attributes information.
*/
class SchemaMap
{

  public:
    static string _DATABLOCK_ID;
    static string _DATABASE_2;
    static string _ENTRY_ID;

    static const int _MAX_LINE_LENGTH = 255;

    static string _STRUCTURE_ID_COLUMN;

    /**
    **  Constructs a schema mapping object.
    **  
    **  \param[in] schemaFile - optional parameter that indicates the name of
    **    the ASCII CIF file, which holds the schema mapping configuration
    **    information. If specified, \e schemaFile file is parsed and the
    **    schema mapping configuration information is taken from it.
    **  \param[in] schemaFileOdb - optional parameter that indicates the name
    **    of the serialized CIF file, which holds the schema mapping
    **    configuration information. If \e schemaFile is not specified,
    **    \e schemaFileOdb is de-serialized and the schema mapping
    **    configuration information is taken from it. If both \e schemaFile
    **    and \e schemaFileOdb are specified, \e schemaFile is parsed,
    **    the schema mapping configuration information is taken from it and
    **    then serialized to \e schemaFileOdb file, at the end of processing.
    **  \param[in] verbose - optional parameter that indicates whether
    **    logging should be turned on (if true) or off (if false).
    **    If \e verbose is not specified, logging is turned off.
    **
    **  \return Not applicable
    **
    **  \pre \e schemaFile and \e schemaFileOdb cannot both be empty.
    **
    **  \post None
    **
    **  \exception: None
    */
    SchemaMap(const string& schemaFile = std::string(),
      const string& schemaFileOdb = std::string(), bool verbose = false);

    SchemaMap(const eFileMode fileMode, const string& dictSdbFileName,
      const bool verbose);

    void Create(const string& op, const string& inFile,
      const string& structId);

    void Write(const string& fileName);

    /**
    **  Destructs a schema mapping object, by releasing all the used resources.
    **
    **  \param: Not applicable
    **
    **  \return Not applicable
    **
    **  \pre None
    **
    **  \post None
    **
    **  \exception: None
    */
    virtual ~SchemaMap();

    /**
    **  Sets the schema revising mode. Schema revising is a process where
    **  the existing schema mapping information is being updated, by examining
    **  the attributes of the converted mmCIF file. Schema revising is needed
    **  in order to increase the length of database attributes in database
    **  schema/data file so that the data can be fully stored in the database.
    **
    **  \param[in] mode - optional parameter that indicates whether
    **    the schema revising should be turned on (if true) or off (if false).
    **    If \e mode is not specified, schema revising is turned on.
    **
    **  \return None
    **
    **  \pre None
    **
    **  \post None
    **
    **  \exception: None
    */
    void SetReviseSchemaMode(bool mode = true);

    /**
    **  Retrieves schema revising mode.
    **
    **  \param: None
    **
    **  \return true - if schema revising is turned on
    **  \return false - if schema revising is turned off
    **
    **  \pre None
    **
    **  \post None
    **
    **  \exception: None
    */
    bool GetReviseSchemaMode();

    /**
    **  Writes the revised schema mapping information to a file.
    **  
    **  \param[in] revisedSchemaFile - indicates the name of
    **    the ASCII CIF file, to which the revised schema mapping information
    **    will be written to.
    **
    **  \return None
    **
    **  \pre None
    **
    **  \post None
    **
    **  \exception: None
    */
    void ReviseSchemaMap(const string& revisedSchemaFile);


    /**
    **  Updates the existing schema mapping object, with the revised
    **  information from the specified schema mapping object.
    **
    **  \param[in] revSchMap - reference to a revised schema mapping object.
    **
    **  \return None
    **
    **  \pre None
    **
    **  \post None
    **
    **  \exception: None
    */
    void updateSchemaMapDetails(SchemaMap& revSchMap);

    /**
    **  In the specified CIF file object, creates tables that are specified in
    **  the schema mapping object.
    **
    **  \param[in] cifFile - reference to a CIF file object that will hold
    **    the data.
    **  \param[in] blockName - the name of the data block
    **
    **  \return None
    **
    **  \pre None
    **
    **  \post None
    **
    **  \exception: None
    */
    void CreateTables(CifFile& cifFile, const string& blockName);

    /**
    **  Retrieves names of all the tables in each data block, defined in
    **  schema mapping object.
    **
    **  \param[out] tablesNames - retrieved tables names
    **
    **  \return None
    **
    **  \pre None
    **
    **  \post None
    **
    **  \exception: None
    */
    void GetAllTablesNames(vector<string>& tablesNames);
    void GetDataTablesNames(vector<string>& tablesNames);

    /**
    **  Utility method, not part of users public API.
    */
    static unsigned int GetTableColumnIndex(const vector<string>& columnsNames,
      const string& colName, const Char::eCompareType compareType =
      Char::eCASE_SENSITIVE);

    ISTable* CreateTableInfo();
    ISTable* CreateColumnInfo();

    void GetAttributeNames(vector<string>& attributes,
      const string& tableName);
    void GetMappedAttributesInfo(vector<vector<string> >& mappedAttrInfo,
      const string& tableName);

    void GetMappedConditions(vector<vector<string> >& mappedConditions,
      const string& tableName);

    void GetTableNameAbbrev(string& abbrevTableName, const string& tableName);
    void GetAttributeNameAbbrev(string& abbrevAttrName, const string& tableName,
      const string& attribName);

    void getSchemaMapDetails(const string& tableName,
      const string& attribName, string& width, string& populated);

    void UpdateAttributeDef(const string& tableName, const string& columnName,
      int type, int iWidth, int newWidth);

    const vector<AttrInfo>& GetTableAttributeInfo(const string& tableName,
      const vector<string>& columnsNames,
      const Char::eCompareType compareType);

    const vector<AttrInfo>& GetAttributesInfo(const string& tableName);

    static bool AreValuesValid(const vector<string>& row,
      const vector<AttrInfo>& aI);
    bool IsTablePopulated(const string& tableName);
    void GetMasterIndexAttribName(string& masterIndexAttribName);

  private:
    bool _verbose;

    string _schemaFile;    // Schema definition file name (mmCIF)
    string _schemaFileOdb; // Schema definition file name (odb)

    CifFile* _fobjS;

    ISTable* _tableSchema;
    ISTable* _attribSchema;
    ISTable* _schemaMap;
    ISTable* _mapConditions;
    ISTable* _tableAbbrev;    
    ISTable* _attribAbbrev;

    ISTable* _attribDescr;
    ISTable* _tableDescr;
    ISTable* _attribView;

    bool _reviseSchemaMode;
    int  _compatibilityMode;

    DicFile* _dict;

    ISTable* _itemTypeTab;
    ISTable* _itemLinkedTab;
    ISTable* _itemDescriptionTab;
    ISTable* _itemExamplesTab;
    ISTable* _categoryTab;
    ISTable* _categoryGroupTab;
    ISTable* _categoryKeyTab;
    ISTable* _itemTab;
    ISTable* _itemAliasesTab;

    string _tableName;
    vector<string> _columnsNames;
    Char::eCompareType _compareType;
    vector<AttrInfo> _aI;
    vector<AttrInfo> _attrInfo;

    void _AssignAttribIndices(void);
    void GetGroupNames(vector<string>& groupNames);

    void Clear();

    void UpdateTableDescr(const string& category);
    void UpdateTableSchema(const string& category);
    void MapAlias(const string& tableName, const string& colName,
      const string& op);
    void GetItemDescription(string& itemDescription,
      const string& itemName);
    void GetFirstItemExample(string& firstItemExample,
      const string& itemName);
    void GetTopParentName(string& topParentName, const string& itemName);
    void GetTypeCode(string& typeCode, const string& topParentName,
      const string& itemName);
    void GetCategoryKeys(vector<string>& categoryKeys,
      const string& category);
    void CreateMappingTables();
    void WriteMappingTables();
    void GetTablesAndColumns(vector<string>& tableList,
      vector<vector<string> >& allColNames, const string& op,
      const string& inFile);
    void UpdateMapConditions();

    void GetDictTables(); 
    void GetColumnsFromDict(vector<string>& colNames,
      const string& tableName);

    void AbbrevTableName(string& dbTableNameAbbrev, const string& tableName);
    void AbbrevColName(const string& tableName, const string& colName);

    void UpdateAttribSchema(const string& tableName, const string& colName,
      const unsigned int itype, const vector<string>& categoryKeys);
    void UpdateAttribView(const string& tableName, const string& colName,
      const unsigned int itype);
    void UpdateAttribDescr(const string& tableName, const string& colName);

    void UpdateSchemaMap(const string& tableName, const string& colName,
      const string& structId);

    unsigned int GetTypeCodeIndex(const string& tableName,
      const string& colName);

    eTypeCode _ConvertDataType(const string& dataType);
};

#endif

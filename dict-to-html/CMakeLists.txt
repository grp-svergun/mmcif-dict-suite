add_executable(dict2HTML
               src/demoGetArgs.C
               src/htmlUtil.C
               src/dict2HTML.C
)

target_include_directories(dict2HTML PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}/include")
target_link_libraries(dict2HTML PRIVATE cif_file_util)

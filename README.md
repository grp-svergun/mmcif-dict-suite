# RCSB mmCIF dictionary suite #

This is a fork of the RCSB mmCIF dictionary suite, with
support added for building with CMake.

The upstream source can be found here:
https://sw-tools.rcsb.org/apps/MMCIF-DICT-SUITE/source.html

The software licence is available at
https://sw-tools.rcsb.org/license.txt
and is reproduced in this repository for convenience.
